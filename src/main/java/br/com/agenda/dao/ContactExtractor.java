package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import br.com.agenda.model.Contact;
import br.com.agenda.model.Phone;

public class ContactExtractor {

	private Connection connection;

	public ContactExtractor(Connection connection) {
		this.connection = connection;
	}

	public Contact extract(ResultSet rs) throws SQLException {
		Contact contact;
		contact = new Contact(rs.getInt("id"), rs.getString("nome"), rs.getString("apelido"), rs.getString("descricao"));

		PhoneDAO phoneDAO = new PhoneDAO(connection);
		List<Phone> telefones = phoneDAO.getAll(contact);

		if (telefones != null)
			contact.setTelefones(telefones);
		return contact;
	}

	@Override
	protected void finalize() throws Throwable {
		if (connection != null)
			connection = null;
		super.finalize();
	}

}
