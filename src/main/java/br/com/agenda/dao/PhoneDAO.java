package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.agenda.model.Contact;
import br.com.agenda.model.Phone;

public class PhoneDAO {

	/**
	 * Constantes para realiza��o de transa��es com o banco de dados. <br>
	 * Usando essas constantes, conseguimos evitar com que usu�rios mal intencionados
	 * usem a t�cnica de SQL Injection.
	 */
	private static final String SQL_INSERT = "insert into telefone(ddd,numero,descricao,id_contato) values (?,?,?,?)";
	private static final String SQL_UPDATE = "update telefone set ddd = ?, numero = ?, descricao = ? where id_telefone = ? and id_contato = ?";
	private static final String SQL_SELECT = "select * from telefone ";
	private static final String SQL_DELETE = "delete from telefone where ? = ?";

	private Connection connection;
	private ContactDAO daoContato = new ContactDAO(Database.getConnection());

	public PhoneDAO(Connection connection) {
		this.connection = connection;
	}

	public PhoneDAO() {
	}

	public void save(Contact contato) throws NullPointerException, SQLException {
		if (contato != null) {
			if (daoContato.isContactExistFrom(contato.getId())) {
				saveAll(contato);
			} else
				throw new NullPointerException("Contato inv�lido.");
		}
	}

	public boolean remove(Contact contato) {
		if (contato != null) {
			if (daoContato.isContactExistFrom(contato.getId())) {
				try (PreparedStatement pst = connection.prepareStatement(SQL_DELETE)) {
					pst.setString(1, "id_contato");
					pst.setInt(2, contato.getId());
					pst.execute();

				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		}
		return false;
	}

	public boolean remove(Phone telefone) throws Exception {
		if (telefone != null) {
			if (this.existeTelefoneCom(telefone.getId())) {
				try (PreparedStatement pst = connection.prepareStatement(SQL_DELETE)) {
					pst.setString(1, "id_contato");
					pst.setInt(2, telefone.getId());
					pst.execute();
				}

			} else
				throw new Exception("Telefone com o ID " + telefone.getId() + " n�o existe.");
		}

		return this.existeTelefoneCom(telefone.getId());
	}

	private void saveAll(Contact contato) throws SQLException {
		for (Phone telefone : contato.getTelefones()) {
			if (telefone.getId() == 0) {
				insert(contato, telefone);
			} else {
				update(contato, telefone);
			}
		}
	}

	private void update(Contact contato, Phone telefone) throws SQLException {
		try (PreparedStatement pst = connection.prepareStatement(SQL_UPDATE)) {
			pst.setString(1, telefone.getDdd());
			pst.setString(2, telefone.getNumero());
			pst.setString(3, telefone.getDescricao());
			pst.setInt(4, telefone.getId());
			pst.setInt(5, contato.getId());
			pst.execute();
		}
	}

	private void insert(Contact contato, Phone telefone) throws SQLException {
		try (PreparedStatement pst = connection.prepareStatement(SQL_INSERT)) {
			pst.setString(1, telefone.getDdd());
			pst.setString(2, telefone.getNumero());
			pst.setString(3, telefone.getDescricao());
			pst.setInt(4, contato.getId());
			pst.execute();
		}
	}

	public boolean existeTelefoneCom(Contact contato) {
		try (PreparedStatement pst = connection.prepareStatement("select count from telefone where id_contato = ?")) {
			pst.setInt(1, contato.getId());
			boolean execute = pst.execute();

			if (execute) {
				ResultSet rs = pst.getResultSet();
				while (rs.next()) {
					int count = rs.getInt("count");
					return count > 0;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Phone search(int idTelefone) {
		return null;
	}

	public Phone search(Contact contact) {
		return null;
	}

	public List<Phone> getAll(Contact contact) {
		List<Phone> phones = new ArrayList<Phone>();

		try (PreparedStatement pst = connection.prepareStatement(" where id_telefone = ?")) {

			pst.setInt(1, contact.getId());
			ResultSet resultSet = pst.executeQuery();

			while (resultSet.next()) {
				phones.add(PhoneExtractor.extract(resultSet));
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Falha na instru��o SQL. \nDetalhes: " + e.getMessage());
		}

		return phones;
	}

	public boolean existeTelefoneCom(int idTelefone) {
		try (PreparedStatement pst = connection.prepareStatement(SQL_SELECT + " where id_telefone = ?")) {
			pst.setInt(1, idTelefone);
			boolean execute = pst.execute();

			if (execute) {
				ResultSet rs = pst.getResultSet();
				while (rs.next()) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
