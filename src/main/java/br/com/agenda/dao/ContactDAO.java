package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import br.com.agenda.model.Contact;

/**
 * Classe respons�vel por realizar as transi��es do JDBC e o banco de dados, tais como
 * inser��o, atualiza��o e remo��o de registros dos contatos
 * 
 * @author Leonardo Braz
 * @see Contact, Phone, PhoneDAO
 * @since 1.6
 */
public class ContactDAO {

	/**
	 * Atributo que ter� a conex�o com o banco de dados aberta.
	 */
	private Connection connection;
	private static final String SQL_INSERT = "insert into contato(nome,apelido,descricao) values(?,?,?)";
	private static final String SQL_SELECT = "select * from contato";
	private static final String SQL_UPDATE = "update contato set nome = ?, apelido = ?, descricao = ? where id_contato = ?";
	private static final String SQL_DELETE = "delete from contato where id_contato = ?";

	public ContactDAO() {
	}

	public ContactDAO(Connection connection) {
		super();
		this.connection = connection;
	}

	/**
	 * M�todo respons�vel por salvar um determinado contato, podendo realizar tanto uma
	 * inser��o como uma atualiza��o no registro. <br>
	 * Para que seja feita uma inser��o, deve ser informado um objeto do tipo Contact sem
	 * um ID, onde o banco de dados far� a gera��o autom�tica da chave prim�ria. No caso
	 * de atualiza��o de registro, o objeto dever� conter o ID de seu respectivo
	 * registro.
	 * 
	 * @param contato
	 *            informando suas informa��es e, os numeros de telefone que ser�o
	 *            vinculados a esse contato.
	 * @throws SQLException
	 *             caso aconte�a algum problema na opera��o.
	 */
	public void save(Contact contato) throws SQLException {
		if (contato != null) {
			// por ser um atributo do tipo int, quando n�o for informado nenhum valor, o
			// padr�o de nulo ser� 0.

			// o tipo primitivo int n�o recebe null como atribui��o.
			if (contato.getId() == 0) {
				insert(contato);

			} else {
				update(contato);
			}
		} else {
			JOptionPane.showMessageDialog(null, "FALHA: Informa��es inv�lidas.");
		}
	}

	/**
	 * M�todo respons�vel por realizar a atualiza��o de determinado registro no banco de
	 * dados. <br>
	 * Esse m�todo � chamado pelo m�todo <code>save</code> presente na classe.
	 * 
	 * @param contato
	 *            informando suas informa��es e, os numeros de telefone que ser�o
	 *            vinculados a esse contato.
	 * @throws SQLException
	 */
	private void update(Contact contato) throws SQLException {
		// buscar o registro sem modifica��o no banco
		Contact busca = this.search(contato.getId());
		if (!busca.equals(contato)) {
			PreparedStatement pst = criaInstrucaoDeUpdate(busca, contato);
			pst.executeUpdate();
		}

	}

	/**
	 * M�todo respons�vel por criar o statement de update para tal registro, verificando
	 * quais informa��es est�o diferentes do objeto original.
	 * 
	 * @param original
	 * @param modificado
	 * @return PreparedStatement pr�-configurado para o update
	 * @throws SQLException
	 */
	private PreparedStatement criaInstrucaoDeUpdate(Contact original, Contact modificado) throws SQLException {
		// Map<String, String>

		Map<Integer, String> valoresParaUpdate = new HashMap<>();

		valoresParaUpdate.put(1, (original.getNome().equals(original.getNome())) ? original.getNome() : modificado.getNome());
		valoresParaUpdate.put(2, (original.getApelido().equals(original.getApelido())) ? original.getApelido() : modificado.getApelido());
		valoresParaUpdate.put(3,
		        (original.getDescricao().equals(original.getDescricao())) ? original.getDescricao() : modificado.getDescricao());

		try (PreparedStatement pst = connection.prepareStatement(SQL_UPDATE)) {
			pst.setString(1, valoresParaUpdate.get(1));
			pst.setString(2, valoresParaUpdate.get(2));
			pst.setString(3, valoresParaUpdate.get(3));
			pst.setInt(4, original.getId());
			return pst;
		}

	}

	/**
	 * M�todo respons�vel por pesquisar um contato a partir do seu c�digo de
	 * indentifica��o.
	 * 
	 * @param id
	 *            identifica��o do registro no banco de dados
	 * @return Contact
	 * @throws SQLException
	 */
	public Contact search(int id) throws SQLException {
		Contact contact = null;

		try (PreparedStatement pst = connection.prepareStatement(SQL_SELECT + " where id_contato = ? ")) {
			pst.setInt(1, id);
			boolean execute = pst.execute();
			if (execute) {
				ResultSet rs = pst.getResultSet();
				if (rs.next()) {
					contact = new ContactExtractor(connection).extract(rs);
				}
			}

		}

		return contact;
	}

	/**
	 * M�todo respons�vel por realizar apenas a inser��o de um registro no banco de
	 * dados. <br>
	 * Esse m�todo � chamado pelo m�todo <code>save</code> presente na classe.
	 * 
	 * @param contato
	 *            informando suas informa��es e, os numeros de telefone que ser�o
	 *            vinculados a esse contato.
	 * @throws SQLException
	 */
	private void insert(Contact contato) throws SQLException {
		try (PreparedStatement pst = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

			pst.setString(1, contato.getNome());
			pst.setString(2, contato.getApelido());
			pst.setString(3, contato.getDescricao());
			pst.execute();

			if (!contato.getTelefones().isEmpty()) {
				ResultSet rs = pst.getGeneratedKeys();

				while (rs.next()) {
					contato.setId(rs.getInt(1));
					System.out.println(contato.getId());
					PhoneDAO telefoneDAO = new PhoneDAO();

					telefoneDAO.save(contato);
				}
			}
		}
	}

	/**
	 * m�todo respons�vel por fazer uma consulta no banco de dados e verificar se existe
	 * um contato com o identificador informado por par�metro.
	 * 
	 * @param id
	 *            identificador do registro
	 * @return true ou false, caso encontre ou n�o o determinado registro.
	 */
	public boolean isContactExistFrom(int id) {
		try (PreparedStatement pst = connection.prepareStatement("select * from contato where id_contato = ?")) {
			pst.setInt(1, id);
			boolean execute = pst.execute();
			if (execute) {
				ResultSet rs = pst.getResultSet();
				while (rs.next()) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * M�todo respons�vel por excluir um contato no banco de dados.
	 * 
	 * @param contato
	 *            que ser� exclu�do
	 * @return resultado da opera��o
	 * @throws SQLException
	 */
	public boolean apaga(Contact contato) throws SQLException {
		if (contato != null) {
			try (PreparedStatement pst = connection.prepareStatement(SQL_DELETE)) {
				pst.setInt(1, contato.getId());
				pst.executeUpdate();
			}
		}
		return this.isContactExistFrom(contato.getId());
	}

	@Override
	protected void finalize() throws Throwable {
		if (connection != null)
			connection = null;
		super.finalize();
	}

}
