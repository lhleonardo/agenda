package br.com.agenda.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.agenda.model.Phone;

public class PhoneExtractor {

	public static Phone extract(ResultSet resultSet) throws SQLException {
		return new Phone(resultSet.getInt("id"), resultSet.getString("ddd"), resultSet.getString("numero"),
		        resultSet.getString("descricao"));
	}

}