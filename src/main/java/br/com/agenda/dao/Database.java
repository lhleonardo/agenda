package br.com.agenda.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

/**
 * Classe respons�vel por realizar apenas as conex�es entre o banco de dados e a
 * aplica��o.
 * 
 * @author Leonardo Braz
 * @since 1.6
 * @see DriverManager, Driver
 */
public class Database {

	/**
	 * M�todo respons�vel por realizar uma conex�o com o banco de dados, sendo informado
	 * as informa��es para a conex�o. <br>
	 * O m�todo atua como uma ponte(JDBC) que conectar� a aplica��o com o banco de dados
	 * (MySQL).
	 * 
	 * @return Connection conex�o pr�-configurada para a utiliza��o.
	 */
	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// System.setProperty("jdbc.Drivers", this.driver);
			con = DriverManager.getConnection("jdbc:mysql://192.168.2.100/agenda", "root", "kkk");

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null,
			        "N�o foi poss�vel conectar-se ao banco de dados. \nFalhaSQL: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null,
			        "N�o foi poss�vel conectar-se ao banco de dados. \nClassNotFound: " + e.getMessage());
		}
		return con;
	}

}