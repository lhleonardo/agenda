package br.com.agenda.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Contact {

	private int id;
	private String nome;
	private String apelido;
	private String descricao;

	private List<Phone> telefones;

	public Contact(String nome, String apelido, String descricao) {
		super();
		this.nome = nome;
		this.apelido = apelido;
		this.descricao = descricao;
		this.telefones = new ArrayList<>();
	}

	public Contact(String nome, String apelido, String descricao, List<Phone> telefones) {
		super();
		this.nome = nome;
		this.apelido = apelido;
		this.descricao = descricao;
		this.telefones = telefones;
	}

	public Contact(int id, String nome, String apelido, String descricao, List<Phone> telefones) {
		super();
		this.id = id;
		this.nome = nome;
		this.apelido = apelido;
		this.descricao = descricao;
		this.telefones = telefones;
	}

	public Contact(int id, String nome, String apelido, String descricao) {
		super();
		this.id = id;
		this.nome = nome;
		this.apelido = apelido;
		this.descricao = descricao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Phone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Phone> telefones) {
		this.telefones = telefones;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addTelefone(Phone telefone) {
		if (telefone != null) {
			this.telefones.add(telefone);
		}
	}

	public String imprimeTelefones() {
		String resultado = "";

		for (Phone telefone : telefones) {
			resultado += telefone + "\n";
		}

		return resultado;
	}

	@Override
	public String toString() {
		return "Contato (id=" + id + ", nome=" + nome + ", apelido=" + apelido + ", descricao=" + descricao
		        + ")";
	}

	public static void main(String[] args) {
		Contact c = new Contact("asdf", "asdfas", "asdfasdf", Arrays.asList(new Phone("33", "12312312",
		        "1241515")));
		System.out.println(c.getId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apelido == null) ? 0 : apelido.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + id;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (apelido == null) {
			if (other.apelido != null)
				return false;
		} else if (!apelido.equals(other.apelido))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id != other.id)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		return true;
	}

}
