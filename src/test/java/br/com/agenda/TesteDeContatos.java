package br.com.agenda;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import javax.swing.JOptionPane;

import br.com.agenda.dao.ContactDAO;
import br.com.agenda.dao.Database;
import br.com.agenda.model.Contact;
import br.com.agenda.model.Phone;

public class TesteDeContatos {

	public static void main(String[] args) {

		try (Connection connection = Database.getConnection()) {
			Phone tel3 = new Phone("69", "9322-3310", "Celular");
			Phone tel4 = new Phone("69", "3424-4460", "Residencial");

			Contact c2 = new Contact("Jo�o da Silva", "Jo�ozinho", "Engenheiro C�vil", Arrays.asList(tel3,
			        tel4));
			
			Contact c1 = new Contact("Leo Braz de Souza","Leo Braz", "Meu pai");
			Phone tel1 = new Phone("69", "9931-9825", "Celular");
			c1.addTelefone(tel1);

			ContactDAO contatoDAO = new ContactDAO(connection);
			contatoDAO.save(c2);
			contatoDAO.save(c1);
		} catch (SQLException e) {
	        // TODO Auto-generated catch block
	        JOptionPane.showMessageDialog(null, e.getMessage());
        }

	}

}
